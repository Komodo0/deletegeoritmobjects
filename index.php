<html>
<head>
    <meta charset="utf-8">

    <link rel="shortcut icon" href="../mainSource/favicon.ico" type="image/x-icon" />

    <script src="../mainSource/jquery-2.2.4/jquery.min.js"></script>

    <script src="../mainSource/moment/moment-with-locales.min.js"></script>

    <script src="../mainSource/bootstrap-3.3.7/js/bootstrap.min.js"></script>

    <script src="../mainSource/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <link href="../mainSource/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="../mainSource/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/i18n/defaults-ru_RU.min.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.format.js"></script>

    <script src="../mainSource/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
	
    <link href="style.css" rel="stylesheet">

    <title>
        RitmTools
    </title>
</head>
<body style="height:100%;">

<div class="container" style="height:100%;">
    <div class="row" style="height:100%;">
		<div class="row" style="text-align:center;">
            <h3>Удаление объектов GeoRitm</h3>
            <small style="position:relative;">Сейчас в системе <span id="freeNums" style="font-size: 18px;"></span> свободных номеров.</small>
			<hr>
		</div>
	<!---
		<section id="auth_screen">
			<div class="row">
				<div class="col-lg-4 col-md-4"></div>
					<div class="col-lg-4 col-md-4">
						<form>
							<div class="well">
								<h4 style="text-align:center;">Авторизация<small><br><br>Используйте аккаунт Мегаплана</small></h4>
								<div class="form-group">
									<label>Имя пользователя:</label>
									<input id="logn" type="text" placeholder="Введите имя пользователя" class="form-control"/>
								</div>
								<div class="form-group">
									<label>Пароль:</label>
									<input id="password" type="password" placeholder="Введите пароль" class="form-control"/>
								</div>
								<div style="text-align:center;">
									<button id="submitLogin" class="btn btn-primary btn-lg">Вход</button>
								</div>
							</div>
						</form>
					</div>
				<div class="col-lg-4 col-md-4"></div>
			</div>
		</section>
		---->

		
		<section id="work_screen">
			<div class="row">
				<div class="col-lg-4 col-md-4" style="height:80%;">
					<div class="well" style="text-align:center;">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<h4>Поиск объекта</h4><hr>
								<input id="objectIdInput" type="text" placeholder="ID объекта" class="form-control"/>
								<br>
								<button id="findObject" class="btn btn-primary btn-lg">Найти</button>
							</div>
						</div>
					</div>
					<div  class="well" style="text-align:center;">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<h4>Результаты поиска</h4>
								
								<div id="objectInfo" style="display:none;">
									<table class="table table-condensed">
											<tr>
												<td>ID в georitm:</td>
												<td><strong id="objectId"></strong></td>
											</tr>
											<tr>
												<td>Реальный ID:</td>
												<td><strong id="objectIdReal"></strong></td>
											</tr>
											<tr>
												<td>Имя объекта:</td>
												<td><strong id="objectName"></strong></td>
											</tr>
											<tr>
												<td>Версия ПО:</td>
												<td><strong id="objectVersion"></strong></td>
											</tr>
											<tr>
												<td>Последний выход GSM:</td>
												<td><strong id="objectLastGSM"></strong></td>
											</tr>
											<tr>
												<td>Последняя точка GPS:</td>
												<td><strong id="objectLastGPS"></strong></td>
											</tr>
                                            <tr>
                                                <td>IMEI:</td>
                                                <td><strong id="objectImei"></strong></td>
                                            </tr>
									</table>
									<button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#confirmDeleteObject">Поставить в очередь</button>
								</div>
								
								<div id="objectNoInfo" class="alert alert-danger">
									Объект с таким <strong>ID</strong> не существует!
								</div>
									
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-4" style="height:80%;">
					<div class="well" style="height:100%; text-align:center;">
						<h4 style="display:inline-block;">Очередь на удаление</h4>
						<div id="floatingCirclesG" class="pull-right">
							<div class="f_circleG" id="frotateG_01"></div>
							<div class="f_circleG" id="frotateG_02"></div>
							<div class="f_circleG" id="frotateG_03"></div>
							<div class="f_circleG" id="frotateG_04"></div>
							<div class="f_circleG" id="frotateG_05"></div>
							<div class="f_circleG" id="frotateG_06"></div>
							<div class="f_circleG" id="frotateG_07"></div>
							<div class="f_circleG" id="frotateG_08"></div>
						</div>
						<div style="position:absolute; left:0; margin-left:35px;">В очереди:<span id="deleteOrderCount">0</span></div>
						<hr>
						<div id="deleteOrder" class="list-group order" style="text-align: left; overflow-y: scroll; height:90%;">
						</div>
					</div>
				</div>

								<div class="col-lg-4 col-md-4" style="height:80%;">
					<div class="well" style="height:100%; text-align:center;">
						<h4 style="display:inline-block;">История удаления</h4>
						<div id="floatingCirclesG">
							<div class="f_circleG" id="frotateG_01"></div>
							<div class="f_circleG" id="frotateG_02"></div>
							<div class="f_circleG" id="frotateG_03"></div>
							<div class="f_circleG" id="frotateG_04"></div>
							<div class="f_circleG" id="frotateG_05"></div>
							<div class="f_circleG" id="frotateG_06"></div>
							<div class="f_circleG" id="frotateG_07"></div>
							<div class="f_circleG" id="frotateG_08"></div>
						</div>
						<hr>
						<div id="deleteHistory" class="list-group order" style="text-align: left; overflow-y: scroll; height:90%;">
						</div>
					</div>
				</div>
				
			</div>
		</section>
	
		
		
    </div>
</div>
	<script src="scripts/cookie.js"></script>
	<script src="scripts/script.js"></script>





<div id="confirmDeleteObject" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
		<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Подтвердите удаление объекта</h4>
      </div>
	  <div class="modal-body">
        <p>Вы уверены, что хотите поставить объект <strong>#<span id="selected_obj_id"></span></strong> в очередь на удаление?</p>
      </div>
      <div class="modal-footer">
        <button onclick="putInOrder();" id="putInOrder" type="button" class="btn btn-secondary">Да</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Отмена</button>
      </div>
    </div>
  </div>
</div>

<div id="objectInfoModal" class="modal fade bd-example-modal-sm"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Информация об объекте</h4>
            </div>
            <div id="objectInfoModalBody" class="modal-body">

            </div>
        </div>
    </div>
</div>

</body>
</html>