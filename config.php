<?php 
function getConfig(){
	$config = array(
	
	//Наш любимый мегаплан
	'MEGAPLAN_URL' => 'megaplan.ritm.ru',

	//Локальная БД для хранения всякого
	'APP_DB_IP' => 'localhost',
	'APP_DB_USERNAME' => 'root',
	'APP_DB_PASSWORD' => 'masterkey',

	//ip георитмовских ресурсов
	'GEORITM_URL' => 'geo.ritm.ru',
	'GEORITM_DB_georitm_URL' => '10.78.11.5',
	'GEORITM_DB_georitm_USERNAME' => 'root',
	'GEORITM_DB_georitm_PASSWORD' => 'masterkey'
	);

	return $config;
}
?>