<?php 
class Action{
	public function routeRequest($request){
				
		if (isset($request) && isset($request['action'])){

	
			switch ($request['action']){
				
				case 'login':
					return $this->login($request);
					break;
					
				case 'logout':
					return $this->logout($request);
					break;
				
				case 'findObject':
					return $this->findObject($request);				
					break;
				
				case 'putInOrder':
					return $this->putInOrder($request);	
					break;
					
				case 'getOrderList':
					return $this->getOrderList($request);	
					break;
					
				case 'getDeletedList':
					return $this->getDeletedList($request);	
					break;

                case 'checkFreeNums':
                    return $this->checkFreeNums($request);
                    break;

				default:
					return false;
			}
			 
		} else {
			return false;
		}
	}
	
	public function login($request){
		//require ''; мегаплан апи
		$login = $request['login'];
		$password = $request['password'];
        return null;
	}
	
	public function logout($request){
		return null;
	}
	
	//Ищем объект по id в БД георитма
	public function findObject($request){
		
		require ('classes/DBconnector.php');
		require ('classes/GeoRitmObject.php');
		require ('config.php');
		
		$objectId = intval($request['objectId']);
		
		$config = getConfig();
		
		$connector = new DBconnector(
			$config['GEORITM_DB_georitm_URL'], 
			$config['GEORITM_DB_georitm_USERNAME'], 
			$config['GEORITM_DB_georitm_PASSWORD']
		);
		
		$georitm_object = new GeoRitmObject();
		$georitm_object->initFromGeoRitmById($objectId, $connector);
		$object_params = $georitm_object->getAsAssocArray();
		$response = json_encode($object_params);
		
		return $response;
	}
	
	public function putInOrder($request){
		
		require ('classes/DBconnector.php');
		require ('classes/GeoRitmObject.php');
		require ('classes/GeoRitmDeleteOrder.php');
        require ('classes/LocalDeleteOrder.php');
		require ('config.php');
		
		$objectId = intval($request['objectId']);
		$objectIdReal = intval($request['objectIdReal']);
		
		$config = getConfig();
		
		$connector = new DBconnector(
			$config['GEORITM_DB_georitm_URL'], 
			$config['GEORITM_DB_georitm_USERNAME'], 
			$config['GEORITM_DB_georitm_PASSWORD']
		);

        $localConnector = new DBconnector(
            $config['APP_DB_IP'],
            $config['APP_DB_USERNAME'],
            $config['APP_DB_PASSWORD']
        );
		
		$georitm_object = new GeoRitmObject();
		$georitm_object->initFromGeoRitmById($objectId, $connector);
		
		if (($georitm_object->getObjectIdReal()) &&
		($georitm_object->getObjectIdReal() == $objectIdReal) &&
		(!GeoRitmDeleteOrder::alreadyInOrder($georitm_object, $connector))){

            $georitm_object->putInLocalDB($localConnector);
            LocalDeleteOrder::putInDeleteHistory($georitm_object, $localConnector);
			$mysqli_result = GeoRitmDeleteOrder::putInDeleteOrder($georitm_object, $connector);


            $response = array(
				'putInOrderResult' => $mysqli_result,
				'error' => 'Ошибка MySQL при добавлении объекта!'
			);
			$response = json_encode($response);
			return $response;
		}
		
		$response = array(
			'putInOrderResult' => false,
			'error' => 'Ошибка! Объект не существует или уже в очереди!'
		);
		$response = json_encode($response);
		
		return $response;
	}
	
	//Возвращаем список объектов в очереди на удаление
	public function getOrderList($request){
		
		require ('classes/DBconnector.php');
		require ('classes/GeoRitmObject.php');
		require ('classes/GeoRitmDeleteOrder.php');
		require ('config.php');
		
		$config = getConfig();
		$connector = new DBconnector(
			$config['GEORITM_DB_georitm_URL'], 
			$config['GEORITM_DB_georitm_USERNAME'], 
			$config['GEORITM_DB_georitm_PASSWORD']
		);

        $localConnector = new DBconnector(
            $config['APP_DB_IP'],
            $config['APP_DB_USERNAME'],
            $config['APP_DB_PASSWORD']
        );
		
		//Нумерованный массив, каждый из элементов - массив [id, object_id]
		$order_list = GeoRitmDeleteOrder::getOrderList($connector);
		$georitm_object = new GeoRitmObject();
		
		$obj_list = array();
		foreach ($order_list as $task){
		    $georitm_object->initFromLocalByRealId($task['object_id'], $localConnector);
			if (!$georitm_object->getObjectId()){
			    //Если вдруг почему то в очереди есть объект, которого нет в локальной базе - инициализируем его из георитма и сразу складываем в локальную базу.
			    $georitm_object->initFromGeoRitmByRealId($task['object_id'], $connector);
			    $georitm_object->putInLocalDB($localConnector);
            };
			$object_as_array = $georitm_object->getAsAssocArray();
			$object_as_array['task_id'] = $task['id'];
			$georitm_object->resetState();
			array_push($obj_list, $object_as_array);
		}
		
		$response = json_encode($obj_list);
		
		return $response;
	}
	
	public function getDeletedList($request){
        require ('classes/DBconnector.php');
        require ('classes/GeoRitmObject.php');
        require ('classes/GeoRitmDeleteOrder.php');
        require ('classes/LocalDeleteOrder.php');
        require ('classes/User.php');
        require ('config.php');

        $config = getConfig();

        $localConnector = new DBconnector(
            $config['APP_DB_IP'],
            $config['APP_DB_USERNAME'],
            $config['APP_DB_PASSWORD']
        );

        //Нумерованный массив, каждый из элементов - массив [id, object_id]
        $order_list = LocalDeleteOrder::getLimitedOrderList($localConnector);
        foreach ($order_list as $key => $task){
            $user = User::getUserById($task['initiator'], $localConnector);
            $name = $user['first_name'] . ' ' . $user['second_name'];
            $order_list[$key]['initiator'] = $name;

        }
        /*$georitm_object = new GeoRitmObject();

        $obj_list = array();
        foreach ($order_list as $task){
            if (!$georitm_object->initFromLocalByRealId($task['object_id'], $localConnector)){
                //Если вдруг почему то в очереди есть объект, которого нет в локальной базе - инициализируем его из георитма и сразу складываем в локальную базу.
                $georitm_object->initFromGeoRitmByRealId($task['object_id'], $connector);
                $georitm_object->putInLocalDB($localConnector);
            };
            $object_as_array = $georitm_object->getAsAssocArray();
            $object_as_array['task_id'] = $task['id'];
            $georitm_object->resetState();
            array_push($obj_list, $object_as_array);
        }*/

        $response = json_encode($order_list);

        return $response;
	}

	//Проверяем количество свободных четырехзнаков в георитме.
	public function checkFreeNums($request){

        require ('classes/DBconnector.php');
        require ('classes/GeoritmUtils.php');
        require ('config.php');
        $config = getConfig();
        $connector = new DBconnector(
            $config['GEORITM_DB_georitm_URL'],
            $config['GEORITM_DB_georitm_USERNAME'],
            $config['GEORITM_DB_georitm_PASSWORD']
        );

        $numsCount = GeoritmUtils::checkFreeNums($connector);

        $response = json_encode($numsCount);

        return $response;

    }

}
?>