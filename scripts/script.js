function login(login, password){
	$.ajax({
			type: 'POST',
			url: 'route.php',
			data: {
				'action': 'login',
				'login': login,
				'password': password
			},
			success: function(data){
				response = JSON.parse(data);
				if (response['logged_in'] == true){
					alert('залогинились');
				} else {
					alert('ошибка логина');
				}
			}
		});	
}

$('#submitLogin').on('click', function(){
	login = $('#login').val();
	password = $('password').val();
	login(login.password);	
});

function createListObject(georitmObject, isActive){
	if (isActive){active = 'active';} else {active = '';}
	if ((georitmObject['objectImei'] === undefined) || (georitmObject['objectImei'] == null)){
        georitmObject['objectImei'] = '-';
    }
	// Приходит массив объектов, в каждом следующие поля:
	// task_id
	// objectId
	// objectIdReal
	// objectName
	// objectVersion
	// objectLastGSM 
	// objectLastGPS
    // objectImei
	result = '\
		<a href="#" id="order_item_' + georitmObject['task_id'] + '" class="list-group-item ' + active + '">	\
			<div>	\
				<h4 class="list-group-item-heading">Объект # ' + georitmObject['objectId'] + '<small class="pull-right">Задача № ' + georitmObject['task_id'] + '</small></h4>	\
			<!---	<div>	\
					<div>Инициатор: ' + georitmObject['initiator'] + '</div>	\
					<div>Поставлен: ' + georitmObject['add_datetime'] + '</div>	\
				</div> --->	\
			</div>	\
			<div class="hiddenObjectInfo" style="display:none;">	\
				<hr style="margin-top:0; margin-bottom:5px;">	\
				<table class="">	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Реальный ID:</p>	\
							</td>	\
							<td>	\
								<strong id="objectIdReal">	\
									<p class="list-group-item-text">' + georitmObject['objectIdReal'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Имя объекта:</p>	\
							</td>	\
							<td>	\
								<strong id="objectName">	\
									<p class="list-group-item-text">' + georitmObject['objectName'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Версия ПО:</p>	\
							</td>	\
							<td>	\
								<strong id="objectVersion">	\
									<p class="list-group-item-text">' + georitmObject['objectVersion'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Последний выход GSM:</p>	\
							</td>	\
							<td>	\
								<strong id="objectLastGSM">	\
									<p class="list-group-item-text">' + georitmObject['objectLastGSM'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Последняя точка GPS:</p>	\
							</td>	\
							<td>	\
								<strong id="objectLastGPS">	\
									<p class="list-group-item-text">' + georitmObject['objectLastGPS'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">IMEI:</p>	\
							</td>	\
							<td>	\
								<strong id="objectImei">	\
									<p class="list-group-item-text">' + georitmObject['objectImei'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
					</table>	\
				</div>	\
		</a>	\
	';
	
	return result;
		
}


function createDeleteListObject(georitmObject, isActive){
    if (isActive){active = 'active';} else {active = '';}
    if ((georitmObject['objectImei'] === undefined) || (georitmObject['objectImei'] == null)){
        georitmObject['objectImei'] = '-';
    }
    // Приходит массив объектов, в каждом следующие поля:
    // task_id
    // objectId
    // objectIdReal
    // objectName
    // objectVersion
    // objectLastGSM
    // objectLastGPS
    // objectImei
    result = '\
		<a href="#" id="order_item_' + georitmObject['task_id'] + '" class="list-group-item ' + '">	\
			<div>	\
				<h4 class="list-group-item-heading">Объект # ' + georitmObject['objectId'] + '<small class="pull-right">Task ID: ' + georitmObject['task_id'] + '</small></h4>	\
				<div>	\
				<!---	<div>Инициатор: ' + georitmObject['initiator'] + '</div>    --->	\
					<div>Поставлен: ' + georitmObject['add_datetime'] + '</div>	\
				</div>	\
			</div>	\
			<div class="hiddenObjectInfo" style="display:none;">	\
				<hr style="margin-top:0; margin-bottom:5px;">	\
				<table class="">	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Реальный ID:</p>	\
							</td>	\
							<td>	\
								<strong id="objectIdReal">	\
									<p class="list-group-item-text">' + georitmObject['objectIdReal'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Имя объекта:</p>	\
							</td>	\
							<td>	\
								<strong id="objectName">	\
									<p class="list-group-item-text">' + georitmObject['objectName'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Версия ПО:</p>	\
							</td>	\
							<td>	\
								<strong id="objectVersion">	\
									<p class="list-group-item-text">' + georitmObject['objectVersion'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Последний выход GSM:</p>	\
							</td>	\
							<td>	\
								<strong id="objectLastGSM">	\
									<p class="list-group-item-text">' + georitmObject['objectLastGSM'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">Последняя точка GPS:</p>	\
							</td>	\
							<td>	\
								<strong id="objectLastGPS">	\
									<p class="list-group-item-text">' + georitmObject['objectLastGPS'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
						<tr>	\
							<td>	\
								<p class="list-group-item-text">IMEI:</p>	\
							</td>	\
							<td>	\
								<strong id="objectImei">	\
									<p class="list-group-item-text">' + georitmObject['objectImei'] + '</p>	\
								</strong>	\
							</td>	\
						</tr>	\
					</table>	\
				</div>	\
		</a>	\
	';

    return result;

}


//Поиск и выведение объекта
$('#findObject').click(function(event){
	
	id = $('#objectIdInput').val();
	
	$.ajax({
		type: 'GET',
		url: 'route.php',
		data: {
			'action': 'findObject',
			'objectId': id
		},
		success: function(data){
			//alert(data);
			response = JSON.parse(data);
			
			if (response['objectId'] == null){
				$('#objectNoInfo').show();
				$('#objectInfo').hide();
			} else {
				$('#objectNoInfo').hide();
				object = $('#objectInfo');
				
				date_border = new Date('2016-01-01 00:00:00');
				date_GSM = new Date(response['objectLastGSM']);
				date_GPS = new Date(response['objectLastGPS']);
				
				dstring_GSM = null;
				if (date_GSM >= date_border){
					dstring_GSM = '<span style="color:red;">' + response['objectLastGSM'] + '</span>';
				} else {
					dstring_GSM = '<span style="color:green;">' + response['objectLastGSM'] + '</span>';
				}
				
				dstring_GPS = null;
				if (date_GPS >= date_border){
					dstring_GPS = '<span style="color:red;">' + response['objectLastGPS'] + '</span>';
				} else {
					dstring_GPS = '<span style="color:green;">' + response['objectLastGPS'] + '</span>';
				}
				
				
				object.show();			
				object.find('#objectId').text(response['objectId']);
				object.find('#objectIdReal').text(response['objectIdReal']);
				object.find('#objectName').text(response['objectName']);
				object.find('#objectVersion').text(response['objectVersion']);
				object.find('#objectLastGSM').html(dstring_GSM);
				object.find('#objectLastGPS').html(dstring_GPS);
                object.find('#objectImei').html(response['objectImei']);
				
				$('#selected_obj_id').text(response['objectId']);
			}
		}
	});
});


//Биндим на Enter поиск объекта.
$(document).bind('keydown', function(){
	if(event.keyCode==13)
       {
          $('#findObject').click();
          return false;
       }
});

//Ставим объект в очередь
function putInOrder(){
	$('#confirmDeleteObject').modal('toggle');
	
	result_id = $('#objectId').text();
	selected_id = $('#selected_obj_id').text();
	result_real_id = $('#objectIdReal').text();
	
	if (result_id == selected_id){
		$.ajax({
			type: 'GET',
			url: 'route.php',
			data: {
				'action': 'putInOrder',
				'objectId': result_id,
				'objectIdReal': result_real_id
			},
			success: function(data){
				response = JSON.parse(data);
				if (response['putInOrderResult'] == true){
					alert('Объект поставлен в очередь!');
				} else {
					alert(response['error']);
				}
			}
		});
		
	}
};

//Открывание-закрывание элементов списков
$('.order').on('click', 'a', function(){
	//$(this).find('.hiddenObjectInfo').slideToggle('fast');
    $('#objectInfoModalBody').html('');
	$(this).clone().addClass('clone').removeClass('active').removeAttr("href").appendTo('#objectInfoModalBody').find('.hiddenObjectInfo').show();
	$('#objectInfoModal').modal('toggle');
});


//Обновляем список очереди на удаление
function refreshOrderList(state){
		$.ajax({
		type: 'GET',
		url: 'route.php',
		data: {
			'action': 'getOrderList',
			'first_id': '',//state.order.first_id,
			'last_id': ''//state.order.last_id
			
		},
		success: function(data){	
			//alert(data);
			order_list = JSON.parse(data);
			// Приходит массив объектов, в каждом следующие поля:
			//
			// objectId
			// objectIdReal
			// objectName
			// objectVersion
			// objectLastGSM 
			// objectLastGPS
			$('#deleteOrderCount').text(order_list.length);
			$('#deleteOrder').html('');
			order_list.forEach(function(item, i, arr){
				listCode = createListObject(item, !i);
				$('#deleteOrder').append(listCode);
			});
			
			
			
		}
	});
}


//Обновляем список удаленных
function refreshDeletedList(state){
		$.ajax({
		type: 'GET',
		url: 'route.php',
		data: {
			'action': 'getDeletedList',
            'first_id': '',//state.history.first_id,
            'last_id': ''//state.history.last_id
		},
		success: function(data){

            deleted_list = JSON.parse(data);

            $('#deleteHistory').html('');
			deleted_list.forEach(function (item, i, arr){
                listCode = createDeleteListObject(item, !i);
                $('#deleteHistory').append(listCode);
            });
			//$('#deleteHistory').text(data);
			//А тут обновляем список удаленных объектов, его придется сразу по умному писать :( Но потом, когда уже локальную БД подключу.
		}
	});
}

//Проверяем количество свободных номеров.
function checkFreeNums() {
    $.ajax({
        type: 'GET',
        url: 'route.php',
        data: {
            'action': 'checkFreeNums'
        },
        success: function(data){
            response = JSON.parse(data);
            $('#freeNums').text(response['numsCount']);
        }
    });
}

$(document).ready(function(){

	//---------
	// Это все на будущее, когда надо будет написать красиво и грамотно.
	//---------
	// //определяем переменные
	state = {
		'order':{
			'first_id': null,
			'last_id': null
		},
		'history':{
			'first_id': null,
			'last_id': null
		}
	};

	deleteCookie('state');
	setCookie('state', state);

	//Бесконечный цикл
	setTimeout(function iteration() {
		state = refreshOrderList(getCookie('state'));
		state = refreshDeletedList(getCookie('state'));
        checkFreeNums();
		setCookie('state', state);
		timerId = setTimeout(iteration, 30000);
	}, 0);

});

