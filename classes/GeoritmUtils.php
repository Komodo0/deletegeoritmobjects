<?php

/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 27.02.2017
 * Time: 13:23
 */
class GeoritmUtils
{

    public static function checkFreeNums($DBconnector){

        $query_result = $DBconnector->executeScript('
			SELECT 
			  9999 - COUNT(ext_id) as numsCount
			FROM 
			  georitm.objects o 
			USE INDEX 
			  (object_ext_id_idx) 
			WHERE 
			  ext_id < 10000;
		');

        return $query_result[0];
    }

}