<?php

class GeoRitmObject{
	
	//Объект гео-ритма.
    private $localId = null;
	private $objectId = null;
	private $objectIdReal = null;
	private $objectName = null;
	private $objectVersion = null;
	private $objectLastGSM = null;
	private $objectLastGPS = null;
	private $objectImei = null;
	
	public function resetState(){
        $this->localId = null;
		$this->objectId = null;
		$this->objectIdReal = null;
		$this->objectName = null;
		$this->objectVersion = null;
		$this->objectLastGSM = null;
		$this->objectLastGPS = null;
		$this->objectImei = null;
	}
	
	public function initFromGeoRitmById($id, $DBconnector){
		$query_result = $DBconnector->executeScript('
			SELECT
				id, ext_id, name, rev, last_gsm_time, last_gps_time, imei
			FROM
				georitm.objects o USE INDEX (object_ext_id_idx)
			INNER JOIN
				georitm.object_state os USE INDEX(PRIMARY)
			ON
				o.id = os.object_id
			WHERE
				o.ext_id = '. $id .';
		');
		$this->setObjectId($query_result[0]['ext_id']);
		$this->setObjectIdReal($query_result[0]['id']);
		$this->setObjectName($query_result[0]['name']);
		$this->setObjectVersion($query_result[0]['rev']);
		$this->setObjectLastGSM($query_result[0]['last_gsm_time']);
		$this->setObjectLastGPS($query_result[0]['last_gps_time']);
		$this->setObjectImei($query_result[0]['imei']);
	}
	
	public function initFromGeoRitmByRealId($id, $DBconnector){
		$query_result = $DBconnector->executeScript('
			SELECT
				id, ext_id, name, rev, last_gsm_time, last_gps_time, imei
			FROM
				georitm.objects o USE INDEX (object_ext_id_idx)
			INNER JOIN
				georitm.object_state os USE INDEX(PRIMARY)
			ON
				o.id = os.object_id
			WHERE
				o.id = '. $id .';
		');
		
		$this->setObjectId($query_result[0]['ext_id']);
		$this->setObjectIdReal($query_result[0]['id']);
		$this->setObjectName($query_result[0]['name']);
		$this->setObjectVersion($query_result[0]['rev']);
		$this->setObjectLastGSM($query_result[0]['last_gsm_time']);
		$this->setObjectLastGPS($query_result[0]['last_gps_time']);
        $this->setObjectImei($query_result[0]['imei']);
	}

    public function initFromLocalByRealId($id, $DBconnector){
        $query_result = $DBconnector->executeScript('
			SELECT
				id as local_id, gr_real_id as id, gr_id as ext_id, gr_name as name, gr_ver as rev, gr_last_GSM as last_gsm_time, gr_last_GPS as last_gps_time, gr_imei as imei
			FROM
				ritm_tools.georitm_object o
			WHERE
				o.gr_real_id = '. $id .';
		');

        if (count($query_result) == 0){
            return false;
        } else {
            $this->setLocalId($query_result[0]['local_id']);
            $this->setObjectId($query_result[0]['ext_id']);
            $this->setObjectIdReal($query_result[0]['id']);
            $this->setObjectName($query_result[0]['name']);
            $this->setObjectVersion($query_result[0]['rev']);
            $this->setObjectLastGSM($query_result[0]['last_gsm_time']);
            $this->setObjectLastGPS($query_result[0]['last_gps_time']);
            if (isset($query_result[0]['imei'])){
                $this->setObjectImei($query_result[0]['imei']);
            }
            return true;
        }

    }

	
	public function getAsAssocArray(){
		$assoc_array = array(
			'objectId' => $this->getObjectId(),
			'objectIdReal' => $this->getObjectIdReal(),
			'objectName' => $this->getObjectName(),
			'objectVersion' => $this->getObjectVersion(),
			'objectLastGSM' => $this->getObjectLastGSM(),
			'objectLastGPS' => $this->getObjectLastGPS(),
            'objectImei' => $this->getObjectImei()
		);
		
		return $assoc_array;
	}

	public function putInLocalDB($DBconnector){

	    if (is_null($this->getObjectImei())){
	        $imei = 'null';
        } else {
	        $imei = $this->getObjectImei();
        }

        $query_result = $DBconnector->executeInsertScript('
			INSERT INTO
				ritm_tools.georitm_object (gr_id, gr_real_id, gr_name, gr_ver, gr_last_GSM, gr_last_GPS, gr_imei)
			VALUES 
				(' . $this->getObjectId() . ", " . $this->getObjectIdReal() . ", '" . $this->getObjectName() . "', '" . $this->getObjectVersion() . "', '" . $this->getObjectLastGSM() . "', '" . $this->getObjectLastGPS() . "', " . $imei .");
		");

        $local_id = $DBconnector->executeScript('
            SELECT
                id
            FROM
                ritm_tools.georitm_object g
            WHERE 
              gr_real_id = ' . $this->getObjectIdReal() . '
        ');

        $local_id = $local_id[0]['id'];

        $this->setLocalId($local_id);

        return $query_result;

    }

	//========================================
	//Геттеры для полей
    public function getLocalId(){
        return $this->localId;
    }

	public function getObjectId(){
		return $this->objectId;
	}
	
	public function getObjectIdReal(){
		return $this->objectIdReal;
	}
	
	public function getObjectName(){
		return $this->objectName;
	}
	
	public function getObjectVersion(){
		return $this->objectVersion;
	}
	
	public function getObjectLastGSM(){
		return $this->objectLastGSM;
	}
	
	public function getObjectLastGPS(){
		return $this->objectLastGPS;
	}

    public function getObjectImei(){
        return $this->objectImei;
    }
	//----------------------------------------
	
	
	
	
	
	//========================================
	//сеттеры для внутреннего использования
    public function setLocalId($value){
        $this->localId = $value;
    }

	private function setObjectId($value){
		$this->objectId = $value;
	}
	
	public function setObjectIdReal($value){
		$this->objectIdReal = $value;
	}
	
	public function setObjectName($value){
		$this->objectName = $value;
	}
	
	public function setObjectVersion($value){
		$this->objectVersion = $value;
	}
	
	public function setObjectLastGSM($value){
		$this->objectLastGSM = $value;
	}
	
	public function setObjectLastGPS($value){
		$this->objectLastGPS = $value;
	}

    private function setObjectImei($value){
        $this->objectImei = $value;
    }
	//----------------------------------------
	
	
}

?>