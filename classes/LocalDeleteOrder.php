<?php

/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 20.02.2017
 * Time: 12:52
 */
class LocalDeleteOrder
{

    static function getLimitedOrderList($DBconnector, $limit = null){

        if (is_int($limit)){
            $limit = 'limit ' . $limit;
        } else {
            $limit = '';
        }

        $query_result = $DBconnector->executeScript('
			SELECT
                dh.id as task_id, initiator, gr_id as objectId, gr_real_id as objectIdReal, gr_name as objectName, gr_ver as objectVersion, gr_last_GSM as objectLastGSM, gr_last_GPS as objectLastGPS, add_datetime
            FROM
                ritm_tools.objects_delete_history dh
            INNER JOIN
                ritm_tools.georitm_object go
            ON
                dh.georitm_object = go.id
            ORDER BY
                task_id
            DESC
                ' . $limit . ';
		');

        return $query_result;
    }

    static function putInDeleteHistory($GeoritmObject, $DBconnector, $initiator = 0){

        $query_result = $DBconnector->executeInsertScript('
            INSERT INTO
                ritm_tools.objects_delete_history (georitm_object, initiator)
            VALUES 
                (' . $GeoritmObject->getLocalId() . ', ' . $initiator . ');
            ');

        return $query_result;
    }

}