<?php

class GeoRitmDeleteOrder{
	
	
	static function getOrderList($DBconnector){
		$query_result = $DBconnector->executeScript('
			SELECT
				id, object_id
			FROM
				georitm.erase_tasks e;
		');
		
		return $query_result;
	}

	static function putInDeleteOrder($GeoritmObject, $DBconnector){
		$query_result = $DBconnector->executeInsertScript('
			INSERT INTO
				georitm.erase_tasks (object_id)
			VALUES 
				(' . $GeoritmObject->getObjectIdReal() . ');
		');
		
		return $query_result;
	}
	
	static function alreadyInOrder($GeoritmObject, $DBconnector){
		$query_result = $DBconnector->executeScript('
			SELECT
				id, object_id
			FROM
				georitm.erase_tasks e
			WHERE
				object_id = ' . $GeoritmObject->getObjectIdReal() . ';
		');
		
		if ($query_result){
			return true;
		} else {
			return false;
		}
	}
	
}

?>