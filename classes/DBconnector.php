<?php 

class DBconnector{

	private $db_ip;
	private $db_login;
	private $db_password;
	
	function __construct($db_ip, $db_login, $db_password){
		$this->db_ip = $db_ip;
		$this->db_login = $db_login;
		$this->db_password = $db_password;
	}
   
	function getIp(){
		return $this->dp_ip;
	}

	function connectionIsOk(){
		$connection = new mysqli($this->db_ip, $this->db_login, $this->db_password);
        if ($connection->connect_errno) {
            return false;
        } else {
			return true;
		}
	}

	function executeScript($request){
		if ($this->connectionIsOk()){
			$result = false;
			$connection = new mysqli($this->db_ip, $this->db_login, $this->db_password);
            $connection->set_charset("utf8");
			$query = $connection->query($request);
			if ($query){
				$num_rows = $query->num_rows;
				$result = array();
				for ($i = 0; $i < $num_rows; $i++){
					$result[$i] = $query->fetch_array(MYSQLI_ASSOC);
				}
				$query->close();
				return $result;
			}
			return $connection->info;
		} else {
			return 'Connection Is Not Ok!';
		}
	}
	
	function executeInsertScript($request){
		if ($this->connectionIsOk()){
			$result = false;
			$connection = new mysqli($this->db_ip, $this->db_login, $this->db_password);
            $connection->set_charset("utf8");
			$result = $connection->query($request);
			return $result;
		} else {
			return 'Connection Is Not Ok!';
		}
	}

}
?>