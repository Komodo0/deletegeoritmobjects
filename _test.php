<?php

	require ('classes/DBconnector.php');
	require ('classes/GeoRitmObject.php');
	require ('classes/GeoRitmDeleteOrder.php');
	require ('config.php');

	$config = getConfig();
		
	$connector = new DBconnector(
		$config['GEORITM_DB_georitm_URL'], 
		$config['GEORITM_DB_georitm_USERNAME'], 
		$config['GEORITM_DB_georitm_PASSWORD']
	);
	
	$georitm_object_1 = new GeoRitmObject();
	$georitm_object_1->initFromGeoRitmById(123456, $connector);
	
	$georitm_object_2 = new GeoRitmObject();
	$georitm_object_2->initFromGeoRitmById(6963, $connector);
	
	if (GeoRitmDeleteOrder::alreadyInOrder($georitm_object_2, $connector)) {
		echo 'TRUE';
	} else {
		echo 'FALSE';
	}
	
?>